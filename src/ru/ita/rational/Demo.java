package ru.ita.rational;

/**
 * Created by ITA on 24.12.2014.
 */
public class Demo {
    public static Rational multiply(Rational number1, Rational number2){
        Rational tmp = new Rational(1,1);
        tmp.setUp(number1.getUp() * number2.getUp());
        tmp.setDown(number1.getDown() * number2.getDown());
        return tmp;
    }
    public static void main(String[] args) {
        Rational fraction1 = new Rational(2, 4);
        Rational fraction2 = new Rational(1, 4);
        Rational result = new Rational(1, 1);
        result = multiply(fraction1,fraction2);
        System.out.println(result);
    }

}
