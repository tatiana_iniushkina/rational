package ru.ita.rational;

/**
 * Created by ITA on 24.12.2014.
 */
public class Rational {
    private int up;
    private int down;

    public Rational(int up, int down) {
        this.up = up;
        this.down = down;
    }

    public int getUp() {
        return up;
    }

    public void setUp(int up) {
        this.up = up;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }

    @Override
    public String toString() {
        return "Rational{" +
                "up=" + up +
                ", down=" + down +
                '}';
    }

}
